import operator
import logging
logging.basicConfig(filename='olymipic.log', level=logging.DEBUG,
                    format='%(asctime)s:%(module)s:%(funcName)s::%(message)s')


def compute_avg_age(age_of_men_in_hwc):
    avg_age_per_season = {}
    for year in age_of_men_in_hwc:
        avg_age_per_season[year] =\
            round(sum(age_of_men_in_hwc[year])/len(age_of_men_in_hwc[year]))
    ordered_avg_age_per_season =\
        dict(sorted(avg_age_per_season.items(),
                    key=operator.itemgetter(0)))
    logging.debug(ordered_avg_age_per_season)
    return ordered_avg_age_per_season


def compute_avg_age_of_athletes_in_mens_heavyweight_boxing(event_records):
    age_of_men_in_hwc_per_year = {}
    for event in event_records:
        if event['Event'] == "Boxing Men's Heavyweight":
            sample_list = []
            year = event['Year']
            age = event['Age']
            if age != 'NA':
                age = int(age)
                age_of_men_in_hwc_per_year[year] =\
                    age_of_men_in_hwc_per_year.get(year, sample_list)
                (age_of_men_in_hwc_per_year[year]).append(age)

    compute_avg_age(age_of_men_in_hwc_per_year)
