import operator
import logging
logging.basicConfig(filename='olymipic.log', level=logging.DEBUG,
                    format='%(asctime)s:%(module)s:%(funcName)s:%(message)s')


def medals_won_by_each_countries(medals_won_raw_data):
    medals_won_by_each_country = {}
    for country, medal in medals_won_raw_data:
        medals_data_sample = {'Gold': 0,
                              'Silver': 0,
                              'Bronze': 0}
        try:
            medals_won_by_each_country[country][medal] += 1
        except KeyError:
            medals_won_by_each_country[country] = medals_data_sample
            medals_won_by_each_country[country][medal] += 1

    return medals_won_by_each_country


def top_10_performers_after_2000(performance_report):
    total_medals_won = {}
    for country in performance_report:
        total_medals_won[country] =\
            performance_report[country]['Gold'] +\
            performance_report[country]['Silver'] +\
            performance_report[country]['Bronze']

    top_10_performers = sorted(total_medals_won.items(),
                               key=operator.itemgetter(1), reverse=True)
    for country, medals in top_10_performers[:10]:
        logging.debug(country)
        logging.debug(performance_report[country])

    return top_10_performers[:10]


def compute_best_performers_after_2000(event_records, year):
    countries_and_medals = []
    for record in event_records:
        season = int(record['Year'])
        medal = record['Medal']
        country = record['Team']
        if season > year:
            if medal != 'NA':
                countries_and_medals.append((country, medal))
    performance_report = medals_won_by_each_countries(countries_and_medals)
    top_performers = top_10_performers_after_2000(performance_report)

    return top_performers
