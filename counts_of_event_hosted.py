import logging
logging.basicConfig(filename='olymipic.log', level=logging.DEBUG,
                    format='%(asctime)s:%(module)s:%(funcName)s::%(message)s')


def compute_number_of_times_each_city_has_hosted_olympics(event_records):

    cities_and_games = {}
    for record in event_records:
        sample_list = []
        event = record['Games']
        city = record['City']
        cities_and_games[city] =\
            (cities_and_games.get(city, sample_list))
        if event not in cities_and_games[city]:
            (cities_and_games[city]).append(event)

    number_of_olympic_seasons_hosted = {}
    for season in cities_and_games:
        number_of_olympic_seasons_hosted[season] =\
            len(cities_and_games[season])
    logging.debug(number_of_olympic_seasons_hosted)

    return number_of_olympic_seasons_hosted
