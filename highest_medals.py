import operator
import logging
logging.basicConfig(filename='olymipic.log', level=logging.DEBUG,
                    format='%(asctime)s:%(module)s:%(funcName)s::%(message)s')


def compute_highest_medals_winner(medal_winners_per_season):
    maximum_medal_achievers = {}
    for year in medal_winners_per_season:
        ordered_yearly_data =\
            dict(sorted(medal_winners_per_season[year].items(),
                        key=operator.itemgetter(1), reverse=True))
        maxm = max(medal_winners_per_season[year].items(),
                   key=operator.itemgetter(1))

        for player in ordered_yearly_data:
            if ordered_yearly_data[player] == maxm[1]:
                maximum = [player, maxm[1]]
                maximum_medal_achievers[year] =\
                    maximum_medal_achievers.get(year, [])
                (maximum_medal_achievers[year]).append(maximum)
            else:
                break
    logging.debug(maximum_medal_achievers)
    return maximum_medal_achievers


def compute_person_who_won_highest_medals_of_each_season(event_records):
    medals_count_per_season_per_person = {}
    for event in event_records:
        if event['Medal'] != 'NA':
            year = event['Year']
            name = event['Name']
            try:
                medals_count_per_season_per_person[year][name] += 1
            except KeyError:
                medals_count_per_season_per_person[year] =\
                    medals_count_per_season_per_person.get(year, {})
                medals_count_per_season_per_person[year][name] =\
                    (medals_count_per_season_per_person[year]).get(name, 0)
                medals_count_per_season_per_person[year][name] += 1
    ordered_medals_count =\
        dict(sorted(medals_count_per_season_per_person.items(),
                    key=operator.itemgetter(0)))

    highest_medals_winner = compute_highest_medals_winner(ordered_medals_count)
    return highest_medals_winner
