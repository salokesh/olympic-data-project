import operator
import logging
logging.basicConfig(filename='olymipic.log', level=logging.DEBUG,
                    format='%(asctime)s:%(module)s:%(funcName)s::%(message)s')


def compute_m_f_ratio(m_f_participation_records):
    m_f_ratio_by_decade = {}
    for decade in m_f_participation_records:
        number_of_males = m_f_participation_records[decade]['M']
        number_of_females = m_f_participation_records[decade]['F']
        minimum_number = min(number_of_males, number_of_females)

        if minimum_number == 0:
            minimum_number = 1

        male_ratio = round(number_of_males/minimum_number)
        female_ratio = round(number_of_females/minimum_number)

        m_f_ratio_by_decade[decade] =\
            {male_ratio: female_ratio}
    ordered_m_f_ratio_by_decade = dict(sorted(m_f_ratio_by_decade.items(),
                                       key=operator.itemgetter(0)))
    logging.debug(ordered_m_f_ratio_by_decade)
    return ordered_m_f_ratio_by_decade


def compute_m_f_participation_ratio_by_decade(event_records):

    participation_record = {}
    for record in event_records:
        year = str(int(record['Year'])//10) + '0'
        sex = record['Sex']
        sample_ratio = {'M': 0, 'F': 0}
        try:
            participation_record[year][sex] += 1
        except KeyError:
            participation_record[year] = sample_ratio
            participation_record[year][sex] += 1
    sex_ratio = compute_m_f_ratio(participation_record)
    return sex_ratio
