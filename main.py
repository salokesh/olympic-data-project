from file_extraction import extract_data

from counts_of_event_hosted import\
    compute_number_of_times_each_city_has_hosted_olympics

from best_performers import\
    compute_best_performers_after_2000

from m_f_ratio import compute_m_f_participation_ratio_by_decade

from avg_age_of_men import\
    compute_avg_age_of_athletes_in_mens_heavyweight_boxing

from medal_winners_india import compute_medal_winners_from_India_each_year

from highest_medals import compute_person_who_won_highest_medals_of_each_season


def main():
    athlete_events = extract_data('athlete_events.csv')

    compute_number_of_times_each_city_has_hosted_olympics(athlete_events)

    compute_best_performers_after_2000(athlete_events, 2000)

    compute_m_f_participation_ratio_by_decade(athlete_events)

    compute_avg_age_of_athletes_in_mens_heavyweight_boxing(athlete_events)

    compute_medal_winners_from_India_each_year(athlete_events)

    compute_person_who_won_highest_medals_of_each_season(athlete_events)


if __name__ == "__main__":
    main()
