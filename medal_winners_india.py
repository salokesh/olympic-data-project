import logging
logging.basicConfig(filename='olymipic.log', level=logging.DEBUG,
                    format='%(asctime)s:%(module)s:%(funcName)s:%(message)s')


def compute_medal_winners_from_India_each_year(event_recors):
    winners_from_India_by_season = {}
    for event in event_recors:
        sample_list = []
        if event['Team'] == 'India':
            if event['Medal'] != 'NA':
                year = event['Year']
                name = event['Name']
                winners_from_India_by_season[year] =\
                    winners_from_India_by_season.get(year, sample_list)
                if name not in winners_from_India_by_season[year]:
                    (winners_from_India_by_season[year]).append(name)

    logging.debug(winners_from_India_by_season)
    return winners_from_India_by_season
