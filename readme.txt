Code python functions that will transform the raw csv data into 
a data structure in a format suitable for plotting with matplotlib.

Generate the following plots ...

1. Number of times each city has hosted the olympics - Piechart
2. Top 10 countries who have won most medals after 2000 - stacked column - split gold/silver/bronze
3. Ratio of M/F participation by decade for all decades - column chart
4. Per olympic year, average age of athletes who participated in Boxing Men’s Heavyweight - Line Chart
5. Find out all medal winners from India per olympic games(year) - Table
6. Create your own scenario
 scenario - find the name of the peroson who won highest medals in all seasons 


Part 2 - Unit Testing:
Rewrite the data project. Create your own smaller dataset. Manually set the result for the unit tests.