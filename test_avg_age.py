import unittest
from file_extraction import extract_data

from avg_age_of_men import\
    compute_avg_age

mock_event_data = extract_data('mock1_events.csv')


class TestAverageAge(unittest.TestCase):

    def test_compute_avg_age(self):

        inputs_and_outputs = [({'2004': [25, 22, 25, 20],
                                '2010': [30, 35, 30, 40]}, {'2004': 23,
                                                            '2010': 34})]
        for inputs, expected_output in inputs_and_outputs:
            output =\
                compute_avg_age(inputs)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
