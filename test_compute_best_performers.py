import unittest
from file_extraction import extract_data

from best_performers import\
    compute_best_performers_after_2000

mock_event_data = extract_data('mock1_events.csv')


class TestTopPerformers(unittest.TestCase):

    def test_top_performers_per_year(self):

        inputs_and_outputs = [((mock_event_data, 1890),
                               [('Denmark/Sweden', 1)])]
        for inputs, expected_output in inputs_and_outputs:
            output =\
                compute_best_performers_after_2000(inputs[0], inputs[1])
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
