import unittest
from file_extraction import extract_data

from counts_of_event_hosted import\
    compute_number_of_times_each_city_has_hosted_olympics

mock_event_data = extract_data('mock1_events.csv')


class TestNumberOfeventsEachCityHosted(unittest.TestCase):

    def test_compute_number_of_events_each_city_hosted(self):

        inputs_and_outputs = [(mock_event_data, {'Barcelona': 1,
                                                 'London': 1,
                                                 'Antwerpen': 1,
                                                 'Paris': 1,
                                                 'Calgary': 1,
                                                 'Albertville': 1,
                                                 'Lillehammer': 1})]
        for inputs, expected_output in inputs_and_outputs:
            output =\
                compute_number_of_times_each_city_has_hosted_olympics(inputs)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
