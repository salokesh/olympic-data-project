import unittest
from file_extraction import extract_data

from highest_medals import\
    compute_person_who_won_highest_medals_of_each_season

mock_event_data = extract_data('mock5_events.csv')
expected_output_data =\
    {'1928': [['Richard James Allen', 1],
              ['Shaukat Ali', 1]],
     '1932': [['Sardar Mohammad Aslam', 1]],
     '1964': [['Syed Mushtaq Ali', 1]],
     '2004': [['Fuad Aslanov', 1]]}


class TestHighestMedalWinners(unittest.TestCase):

    def test_highest_medal_winners(self):

        inputs_and_outputs =\
            [((mock_event_data), expected_output_data)]
        for inputs, expected_output in inputs_and_outputs:
            output =\
                compute_person_who_won_highest_medals_of_each_season(inputs)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
