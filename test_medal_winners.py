import unittest
from file_extraction import extract_data

from medal_winners_india import\
    compute_medal_winners_from_India_each_year

mock_event_data = extract_data('mock5_events.csv')


class TestMedalWinners(unittest.TestCase):

    def test_medal_winners(self):

        inputs_and_outputs =\
            [((mock_event_data), {'1964': ['Syed Mushtaq Ali'],
                                  '1928': ['Richard James Allen',
                                           'Shaukat Ali'],
                                  '1932': ['Sardar Mohammad Aslam']})]
        for inputs, expected_output in inputs_and_outputs:
            output =\
                compute_medal_winners_from_India_each_year(inputs)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

