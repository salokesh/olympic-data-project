import unittest
from file_extraction import extract_data

from m_f_ratio import\
    compute_m_f_participation_ratio_by_decade

mock_event_data = extract_data('mock1_events.csv')


class TestSexRatio(unittest.TestCase):

    def test_compute_m_f_ratio(self):

        inputs_and_outputs = [(mock_event_data, {'1900': {1: 0},
                                                 '1920': {1: 0},
                                                 '1980': {0: 2},
                                                 '1990': {1: 1},
                                                 '2010': {1: 0}})]
        for inputs, expected_output in inputs_and_outputs:
            output =\
                compute_m_f_participation_ratio_by_decade(inputs)
            self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
